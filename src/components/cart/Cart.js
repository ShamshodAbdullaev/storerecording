import React, {Component} from 'react';
import Title from "../Title";
import CardColumns from "./CartColumns";
import EmptyCart from "./EmptyCart";
import {ProductConsumer} from "../../context";
import CartList from "./CartList";
import CartTotals from "./CartTotals";


class Cart extends Component {
    render() {
        return (
            <section>
                <ProductConsumer>
                    {value => {
                        const {cart} = value;
                        if (cart.length){
                            return(
                                <React.Fragment>
                                    <Title name="your" title="cart"/>
                                    <CardColumns/>
                                    <CartList value={value}/>
                                    <CartTotals value={value}/>
                                </React.Fragment>
                            );
                        }else {
                            return (
                                <EmptyCart/>
                            )
                        }
                    }}
                </ProductConsumer>


            </section>
        );
    }
}

// Cart.propTypes = {};

export default Cart;