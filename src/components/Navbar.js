import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {faCartPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import logo from "../logo.svg";
import {ButtonContainer} from "./Button";
import {NavWrapper} from "./NavWrapper";


class Navbar extends Component {
    render() {

        return (

            <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5">
                <Link to='/'>
                    <img src={logo} alt="store" className="navbar-brand"/>
                </Link>
                <ul className="navbar-nav align-items-center">
                    <li className="nav-item ml-5 ">
                        <Link to="/" className="nav-link">
                            <h4 className="text-white">products</h4>
                        </Link>
                    </li>
                </ul>
                <Link to='/cart' className="ml-auto">
                    <ButtonContainer>
                        <span className="mr-2">
                            <FontAwesomeIcon icon={faCartPlus}/>
                        </span>
                        my cart
                    </ButtonContainer>
                </Link>
            </NavWrapper>
        );
    }
}

Navbar.propTypes = {};

export default Navbar;